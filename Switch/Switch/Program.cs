﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SqlerCSharp.Ch06_02
{
	//열거형 선언
	public enum Country { Korea, China, Japan };

	class Program
	{
		static void Main(string[] args)
		{
			Country myCountry = Country.Korea;

			switch (myCountry)
			{
				case Country.Korea:
					Console.WriteLine("한국");
					break;
				case Country.China:
					Console.WriteLine("일본");
					break;
				case Country.Japan:
					Console.WriteLine("중국");
					break;
				default:                //Country 형식에 속하는 항목이 없을 경우
					Console.WriteLine("선택된 나라가 없습니다.");
					break;
			}
		}
	}
}