﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SqlerCSharp.Ch10_01
{
	class Program
	{
		static void Main(string[] args)
		{
			int number1 = 1;
			int number2 = 123;

			Console.WriteLine("number 1 : |{0,5}|, number2 : {1:0000#}"
				, number1, number2);        //number 1 : | 1|, number2 : 00123
			Console.WriteLine("number 1 : |{0,-5}|, number2 : {1:0000#}"
				, number1, number2);        //number 1 : |1 |, number2 : 00123

			int number3 = 12345;
			Console.WriteLine("number 3 : {{ {0} }}", number3); // number 3 : { 12345 }
		}
	}
}