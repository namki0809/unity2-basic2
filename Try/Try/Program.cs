﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SqlerCSharp.Ch08_01
{
	class Program
	{
		static void Main(string[] args)
		{
			Foo("123");        //올바르게 처리
			Foo(null);         //ArgumentNullException 발생
			Foo("일이삼");     //Exception 발생
		}

		static void Foo(string data)
		{
			//복수개의 catch문이 올 수도 있다.
			try
			{
				int number = Int32.Parse(data);
				Console.WriteLine("number : {0}", number);
			}
			catch (ArgumentNullException ex)    //입력 인자가 없을 경우
			{
				Console.WriteLine("ArgumentNullException 처리 : {0}", ex.Message);
			}
			catch (Exception ex)                //최상위 예외 형식
			{
				Console.WriteLine("Exception 처리 : {0}", ex.Message);
			}
		}
	}
}