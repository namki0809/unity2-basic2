﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SqlerCSharp.Ch07_05
{
	class Program
	{
		static void Main(string[] args)
		{
			int result = 0;

			for (int i = 1; i <= 100; i++)
			{
				if (i > 10)             //i가 10보다 크면 for문을 빠져 나간다.
				{
					break;
				}

				if ((i % 2) == 0)       //짝수이면 처리를 건너 뛴다.
				{
					continue;
				}

				result += i;
			}

			Console.WriteLine("break-continue 1~10 중 홀수만 더하기 : {0}", result);
		}
	}
}