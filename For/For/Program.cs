﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SqlerCSharp.Ch07_01
{
	class Program
	{
		static void Main(string[] args)
		{
			int result = 0;

			for (int i = 1; i <= 10; i++)
			{
				result += i;         //result = result + i; 축약 표현
			}

			Console.WriteLine("for문 1~10 더하기 : {0}", result);
		}
	}
}