﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SqlerCSharp.Ch07_06
{
	class Program
	{
		static void Main(string[] args)
		{
			int result = 0;

			for (int i = 1; i <= 10; i++)
			{
				result += i;

				if (i == 5)
				{
					goto Jump;  //Jump 라벨로 이동
				}
			}

			Console.WriteLine("for문 1~10 더하기 : {0}", result);

		Jump:       //goto 문이 이동 할 라벨 지정
			Console.WriteLine("goto 점프 : {0}", result);
		}
	}
}